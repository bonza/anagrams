import pytest
from anagrams import letters_and_numbers as lnn


# initialise a `Letters` object for testing
letters_obj = lnn.Letters(dictionary_path=None)
# override the default dictionary with a fake test-friendly one
letters_obj.dictionary = ['a', 'at', 'cat', 'dag', 'dog']

# output tests for the `Letters` class
@pytest.mark.parametrize('letters,min_length,max_length,expected', [
    # no letters provided
    ('', 2, 10, []),
    # multiple letters provided, no matches
    ('stuvwxyz', 2, 10, []),
    # multiple letters provided, no matches, length-0 'words' are accepted
    ('stuvwxyz', 0, 10, ['']),
    # one letter provided, but length-2 words or greater allowed
    ('a', 2, 10, []),
    # one letter provided, length-1 words are accepted
    ('a', 1, 10, ['a']),
    # two letters provided, length-1 words are accepted
    ('ta', 1, 10, ['a', 'at']),
    # two letters provided in reversed order, length-1 words accepted
    ('at', 1, 10, ['a', 'at']),
    # more than two letters
    ('acdgot', 1, 10, ['a', 'at', 'cat', 'dag', 'dog']),
    # more than two letters, but maximum allowed word length is 2
    ('acdgot', 1, 2, ['a', 'at']),
])
def test_letters_class(letters, min_length, max_length, expected):
    """
    Basic output tests for the `Letters` class.
    """
    output = letters_obj.solve(
        letters, min_length=min_length, max_length=max_length
    )
    assert(output == expected)


# initialise a `Numbers` object for testing
numbers_obj = lnn.Numbers()

# output tests for the `Numbers` class
@pytest.mark.parametrize('numbers,target,expected', [
    # single number given, target attainable
    ([1], 1, {'equation': '1', 'result': 1}),
    # single number given, target not attainable
    ([1], 2, {'equation': '1', 'result': 1}),
    # two identical numbers given, target attainable
    ([1, 1], 2, {'equation': '(1+1)', 'result': 2}),
    # two identical numbers given, target not attainable
    ([1, 1], 3, {'equation': '(1+1)', 'result': 2}),
    # more than two numbers given, target attainable
    ([1, 1, 3, 6, 10], 85, {'equation': '((((1+3)+10)*6)+1)', 'result': 85}),
    # more than two numbers given, target not attainable
    ([1, 1, 3, 6, 10], 86, {'equation': '((((1+3)+10)*6)+1)', 'result': 85}),
])
def test_numbers_class(numbers, target, expected):
    """
    Basic output tests for the `Numbers` class.
    """
    output = numbers_obj.solve(numbers, target)
    assert(output == expected)