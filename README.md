# About

This package is just a bit of fun, which I wrote to get some practice at making
recursive functions. It includes two distinct functions: the first figures out all
possible words from a user-specified set of letters (hence the name 'anagrams'); 
the second takes a list of numbers, and finds the particular arithmetic combination 
of some or all of those numbers, whose result is as close as possible to a specified 
target number.


# Usage

The `letters_and_numbers` module contains two basic classes: `Letters` and `Numbers`.

The `Letters` class is used to find all words contained in a given dictionary
which can be constructed from a user-specified string of characters. Two methods
are available to the user:
- `.set_dictionary()`: this allows the user to specify a path to their own 
  dictionary, as opposed to the default one which comes with the package. This
  should be a simple .txt file or similar, containing words.
- `.solve()`: this solves the anagram problem, and returns a list of valid words.
  The user must provide this method a single string of characters, e.g. `'abcdef'`.
    
The `Numbers` class is used to find the particular arithmetic combination of a 
list of integers whose result is as close as possible to a specified 
target number. It only uses the operations `+`, `-`, `*`, and `/`. The `Numbers`
class offers a single method:
- `.solve()`: takes a list of numbers (integers) and a target number, and determines the
  arithmetic combination of those numbers whose result is as close as possible to
  the target. Returns a dictionary containing the equation, represented as a 
  string, and the result of that equation.