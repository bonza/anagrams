from operator import add, sub, mul, truediv
from anagrams.config import DICT_PATH


class Letters:
    """
    """
    def __init__(self, dictionary_path=DICT_PATH):
        # set the dictionary as default unless otherwise specified
        self.set_dictionary(dictionary_path)
        
    def set_dictionary(self, path):
        """
        """
        if not path:
            self.dictionary = []
        else:
            # read in the dictionary from the specified path into a list of words
            f = open(path, 'r')
            self.dictionary = f.read().split()
        
    def check_is_anagram(self, word, letters):
        """
        Recursive function which checks if `word` can be made out of any subset
        of letters contained within `letters`.
        
        Args:
            word (string):
            letters (string):
                
        Returns:
            A bool, corresponding to whether `word` is or isn't an anagram of
            `letters`
        """
        # if `word` is empty, then 
        if word == '':
            return True
        # if first letter in `word` is not in `letters`, not an anagram
        elif word[0] not in letters:
            return False
        # recursively run the function with the remaining letters
        else:
            i = letters.index(word[0])
            return self.check_is_anagram(word[1:], letters[0:i]+letters[i+1:])
        
    def solve(self, letters, min_length=2, max_length=10):
        """
        Args:
            letters (string):
            min_length (int, optional):
            max_length (int, optional):
                
        Returns:
            List of all possible words (strings) which can be formed using the
            letters in `letters`
        """
        # initiate empty list for solutions
        results = [''] if min_length == 0 else []
        # run through every word in `self.dictionary`, and check if 
        for word in self.dictionary:
            # first enforce word length restrictions on potential results
            length = len(word)
            if (length >= min_length) and (length <= max_length):
                # add word to results list if it is an anagram of `letters`
                if self.check_is_anagram(word, letters):
                    results.append(word)
        return results
    
    
class Numbers:
    """
    """
    # dictionary for arithmetic operators, map between string and function
    OP_DICT = {'+': add, '-': sub, '*': mul, '/': truediv}
    
    def solution_generator(
            self, numbers, target, init=None, seq=None, 
            best_soln={'result': None, 'equation': None}):
        """
        Returns a generator object which yields 'best solution' dictionaries,
        containing 
        
        Args: 
            numbers (list): the list of numbers used to obtain a result
            target (float or int): the target value to get as close as
                possible to by arithmetically combining the elements of 
                `numbers`
            init (float or int):
            seq (string):
            best_soln (dict):
                
        Returns:
            Generator object yielding dictionaries in the form of `best_soln`
        """
        # if origin is None then assign first value from `numbers` and commence
        # recursion
        if init is None:
            # do this with each number in turn, remove number from `numbers`
            for i in range(len(numbers)):
                number = numbers[i]
                seq = str(number)
                best_soln['result'] = number
                best_soln['equation'] = seq
                # remove `number` from `numbers` and recurse with new arguments
                numbers_new = numbers.copy()
                del numbers_new[i]
                yield from self.solution_generator(
                    numbers_new, target, init=number, seq=seq, 
                    best_soln=best_soln
                )    
        # if list is expended (empty) yield the best result    
        elif not numbers:
            yield best_soln
        # main recursive block - runs over until above elif is met   
        else:
            for i in range(len(numbers)):
                number = numbers[i]
                for op in self.OP_DICT:
                    # make arithmetic operation on previous origin and next 
                    # value, assign this as the new origin
                    init_new = self.OP_DICT[op](init, number)
                    # corresponding new sequence following operation
                    seq_new = '(' + seq + op + str(number) + ')'
                    # if the new origin is closer to `target` than the previous 
                    # closest result from 
                    if abs(init_new-target) < abs(best_soln['result']-target):
                        best_soln['result'] = init_new
                        best_soln['equation'] = seq_new
                    # remove number from `numbers` and recurse with new 
                    # reduced arguments
                    numbers_new = numbers.copy()
                    del numbers_new[i]
                    yield from self.solution_generator(
                        numbers_new, target, init=init_new, seq=seq_new, 
                        best_soln=best_soln
                    )
        
    def solve(self, numbers, target):
        """
        Finds the optimal arithmetic combination of the elements of `numbers`, 
        in order to achieve a result as close as possible to `target`.
        
        Args:
            numbers (list): the list of numbers used to obtain a result
            target (float or int): the target value to get as close as
                possible to by arithmetically combining the elements of 
                `numbers`
        
        Returns:
            best_soln (dict): containing the following keys:
                - `result` (float): the closest possible solution to `target`
                - `equation` (string): string representation of the
                    corresponding equation
        """
        # make a solution generator
        sg = self.solution_generator(numbers, target)
        # first potential best solution, needs to be copy
        best_soln = next(sg).copy()
        # if by chance our first solution matches `target`, we're done
        if best_soln['result'] == target:
            return best_soln
        # otherwise loop through possible solutions to find the best
        while True:
            try:
                # next potential solution
                soln = next(sg).copy()
                # if potential solution matches `target`, we're done
                if soln['result'] == target:
                    return soln
                # if better than previous best, replace
                elif abs(soln['result']-target) < abs(best_soln['result']-target):
                    best_soln = soln
            # keep doing this loop until iterator runs out, return the best
            # solution
            except StopIteration:
                break            
        return best_soln
        